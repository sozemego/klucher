# Klucher #
![promo_3.png](https://bitbucket.org/repo/kLGgKL/images/3976070418-promo_3.png)

A website (built using Spring Boot), which allows users to post their thoughts, follow eachother, explore hashtags and (eventually) join chat rooms which are based on popular hashtags.

## Live version ##

Live version can be found at: [klucher.pl](http://klucher.pl). It's hosted on AWS Elastic Beanstalk (one instance at the moment). It also uses their DNS services (Route 53). Feel free to make an account, but permanent purges of the database are to be expected this early in development. Accounts are free, no email required and your password is hashed.

## License ##

[MIT License](https://en.wikipedia.org/wiki/MIT_License)